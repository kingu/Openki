import './ical';
import './i18next';
import './routes';

// import locale file(s)
import 'moment/locale/ar';
import 'moment/locale/de';
import 'moment/locale/es';
import 'moment/locale/fr';
import 'moment/locale/it';
import 'moment/locale/zh-cn';
import 'moment/locale/zh-tw';
import 'moment/locale/da';
import 'moment/locale/ja';
import 'moment/locale/fa';
import 'moment/locale/el';
import 'moment/locale/tr';
import 'moment/locale/hu';
import 'moment/locale/pt';
import 'moment/locale/ko';
